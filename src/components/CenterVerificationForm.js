import React, { Component } from "react";
import { View, Text, TextInput, Keyboard } from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';

import AlertModal from '../components/Common/AlertModal';
import { verifyCenter } from '../actions/verificationActions';

class CenterVerificationForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      centerID: "",
      assessorID: "",
      assessorPassword: "",
      error: false,
      validationError: false,
      errorMsg: ""
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { error, errorMsg } = this.props;
    if (error && error !== prevProps.error) {
      this.setState({ validationError: true, errorMsg });
    }
  }

  onChangeCenterID = (centerID) => {
    this.setState({ centerID });
  }

  onChangeAssessorID = (assessorID) => {
    this.setState({ assessorID });
  }

  onChangeAssessorPassword = (assessorPassword) => {
    this.setState({ assessorPassword });
  }

  verifyCentre = () => {
    const { centerID, assessorID, assessorPassword } = this.state;

    this.setState({ error: false });
    Keyboard.dismiss();

    if (!centerID.trim() && !assessorID.trim() && !assessorPassword.trim()) {
      this.setState({ error: true });
      return;
    }

    const verifyCenterObj = {
      centerID,
      assessorID,
      assessorPassword
    }

    this.props.verifyCenter(verifyCenterObj);
  }

  render() {
    return (
      <View style={{ width: '60%' }}>
        <View style={[styles.fieldContainer, { flexDirection: 'row' }]}>
          <View style={styles.label}>
            <Text>Center ID</Text>
          </View>
          <View style={{ flex: 0.5 }}>
            <TextInput
              placeholder="Enter Here..."
              style={{ height: 40 }}
              onChangeText={this.onChangeCenterID}
              value={this.state.centerID}
            />
          </View>
        </View>
        <View style={[styles.fieldContainer, { flexDirection: 'row' }]}>
          <View style={styles.label}>
            <Text>Assessor ID</Text>
          </View>
          <View style={{ flex: 0.5 }}>
            <TextInput
              placeholder="Enter Here..."
              style={{ height: 40 }}
              onChangeText={this.onChangeAssessorID}
              value={this.state.assessorID}
            />
          </View>
        </View>
        <View style={[styles.fieldContainer, { flexDirection: 'row' }]}>
          <View style={styles.label}>
            <Text>Assessor Password</Text>
          </View>
          <View style={{ flex: 0.5 }}>
            <TextInput
              placeholder="Enter Here..."
              style={{ height: 40 }}
              onChangeText={this.onChangeAssessorPassword}
              value={this.state.assessorPassword}
            />
          </View>
        </View>
        {this.state.error && 
          (
            <View style={styles.errorContainer}>
              <Text style={{ color: 'red' }}>
                Please enter all the fields.
              </Text>
            </View>
          )
        }
        <View style={styles.buttonContainer}>
          <Button 
            buttonStyle={styles.button}
            title="Verify Center"
            onPress={this.verifyCentre}
          />
        </View>

        {this.state.validationError && 
          <AlertModal 
            type="One"
            label="Error" 
            message={this.state.errorMsg}
            onPress={() => this.setState({ validationError: false })} 
          />
        }
      </View>
    )
  }
}


const mapStateToProps = ({ verification }) => {
  return {
    error: verification.error,
    errorMsg: verification.errorMsg
  }
}

export default connect(mapStateToProps, { verifyCenter })(CenterVerificationForm);

const styles = {
  fieldContainer: {
    borderColor: '#D3D3D3',
    borderWidth : 1,
    elevation: 0.5
  },
  label: {
    flex: 0.5,
    paddingTop: 8,
    paddingLeft: 10,
    paddingRight: 10
  },
  buttonContainer: {
    marginTop: 20
  },
  button: {
    backgroundColor: '#2975a0',
    borderRadius: 20,
    paddingTop: 15,
    paddingBottom: 15
  },
  errorContainer: {
    alignItems: 'center',
    marginTop: 20
  }
}