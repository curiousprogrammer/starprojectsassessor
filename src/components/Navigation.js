import React from 'react';
import { Text } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';

import LoginScreen from '../screens/LoginScreen';
import CenterVerificationScreen from '../screens/CenterVerificationScreen';
import TestScreen from '../screens/TestScreen';
import Camera from './Common/Camera';
import ImageScreen from '../screens/ImageScreen';
import TestInfoScreen from '../screens/TestInfoScreen';
import DashboardScreen from '../screens/Dashboard/DashboardScreen';
import StudentList from '../screens/Dashboard/StudentList';
import UploadDataList from '../screens/Dashboard/UploadDataList';
import AnnexureDetailsScreen from '../screens/Dashboard/AnnexureDetailsScreen';

const DashboardStack = createMaterialBottomTabNavigator({
  Home: {
    screen: DashboardScreen,
    navigationOptions: {
      tabBarLabel: <Text style={{ fontSize: 14 }}>Home</Text>,
      tabBarIcon: ({ focused }) => (
        <Icon name="home" size={30} color="#fff" style={{ width: 30, marginTop: -10 }} />
      )
    },
  },
  StudentList: {
    screen: StudentList,
    navigationOptions: {
      tabBarLabel: <Text style={{ fontSize: 14 }}>Assessment</Text>,
      tabBarIcon: ({ focused }) => (
        <Icon name="list-alt" size={30} color="#fff" style={{ width: 30, marginTop: -10 }} />
      )
    },
  },
  UploadDataList: {
    screen: UploadDataList,
    navigationOptions: {
      tabBarLabel: <Text style={{ fontSize: 14 }}>Upload</Text>,
      tabBarIcon: ({ focused }) => (
        <Icon name="upload" size={30} color="#fff" style={{ width: 30, marginTop: -10 }} />
      )
    },
  },
  AnnexureDetails: {
    screen: AnnexureDetailsScreen,
    navigationOptions: {
      tabBarLabel: <Text style={{ fontSize: 14 }}>Annexure</Text>,
      tabBarIcon: ({ focused }) => (
        <Icon name="wpforms" size={30} color="#fff" style={{ width: 30, marginTop: -10 }} />
      )
    },
  },
}, {
  shifting: false,
  labeled: true,
  activeColor: '#ffffff',
  inactiveColor: '#ffffff',
  barStyle: { backgroundColor: '#2975a0', paddingTop: 10 },
});

const RootStack = createStackNavigator({
  Login: LoginScreen,
  Verification: CenterVerificationScreen,
  Dashboard: DashboardStack,
  Camera: Camera,
  Image: ImageScreen,
  TestInfo: TestInfoScreen,
  Test: TestScreen
},{
  headerMode: 'none'
});

const AppContainer = createAppContainer(RootStack);

export default AppContainer;