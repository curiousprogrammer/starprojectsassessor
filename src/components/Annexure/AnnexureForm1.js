import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { Card, Input, Button } from 'react-native-elements';

class AnnexureForm1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstGroup: [
        {"label": "Assessment Agency Name", key: "agency_name"},
        {"label": "Assessor's Aadhar Number", key: "assessor_adhar_number"},
        {"label": "Training Partner Name", key: "traning_partner_name"},
        {"label": "Center SPOC Name", key: "spoc_name"},
        {"label": "Center Id", key: "center_id"},
        {"label": "Job role for which asessment conducted", key: "job_role"},
        {"label": "Student preferred language of assessment", key: "student_language_for_assessment"},
        {"label": "Center address on SDMS", key: "center_address"},
        {"label": "Actual Center Address", key: "actual_center_address"}
      ],
      secondGroup: [
        {"label": "Assessor's Name", key: "assessor_name"},
        {"label": "No of candidates in the batch", key: "no_of_candi_in_batch"},
        {"label": "Training Center Name", key: "traning_center_name"},
        {"label": "SPOC Mobile Number", key: "spoc_mobile_no"},
        {"label": "Batch Id", key: "batch_id"},
        {"label": "Sector Name", key: "sector_name"},
        {"label": "Asessment conducted in the language", key: "assement_language"},
        {"label": "Actual Date of Assessment", key: "actual_date"}
      ],
      agency_name: "",
      assessor_name: "",
      assessor_adhar_number: "",
      no_of_candi_in_batch: "",
      traning_partner_name: "",
      traning_center_name: "",
      spoc_name: "",
      spoc_mobile_no: "",
      center_id: this.props.centerID,
      batch_id: this.props.batchID,
      job_role: "",
      sector_name: "",
      student_language_for_assessment: "",
      assement_language: "",
      center_address: "",
      actual_date: "",
      actual_center_address: "",
      error: false
    }
  }

  onChangeField = (value, fieldName) => {
    this.setState({ [fieldName]: value });
  }

  goToStep2 = () => {
    const { 
      agency_name, 
      assessor_name, 
      assessor_adhar_number, 
      no_of_candi_in_batch, 
      traning_partner_name, 
      traning_center_name,
      spoc_name,
      spoc_mobile_no,
      center_id,
      batch_id,
      job_role,
      sector_name,
      student_language_for_assessment,
      assement_language,
      center_address,
      actual_date,
      actual_center_address
    } = this.state;

    this.setState({ error: false });

    // if (!agency_name.trim() || !assessor_name.trim() || !assessor_adhar_number.trim() || !no_of_candi_in_batch.trim() || !traning_partner_name.trim() || !traning_center_name.trim() || !spoc_name.trim() || !spoc_mobile_no.trim() || !center_id.trim() || !batch_id.trim() || !job_role.trim() || !sector_name.trim() || !student_language_for_assessment.trim() || !assement_language.trim() || !center_address.trim() || !actual_date.trim() || !actual_center_address.trim()) {
    //   this.setState({ error: true });
    //   return;
    // }

    const step1Details = {
      agency_name,
      assessor_name, 
      assessor_adhar_number, 
      no_of_candi_in_batch, 
      traning_partner_name, 
      traning_center_name,
      spoc_name,
      spoc_mobile_no,
      center_id,
      batch_id,
      job_role,
      sector_name,
      student_language_for_assessment,
      assement_language,
      center_address,
      actual_date,
      actual_center_address
    }

    this.props.goToStep2(step1Details);
  }

  renderAccessorDetails() {
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <View style={styles.field}>
          {this.state.firstGroup.map((item, index) => {
            if (item.label === "Center Id") {
              return (
                <View key={index}>
                  <Input 
                    placeholder={item.label}
                    inputContainerStyle={{ borderBottomColor: '#f0f1f2' }}
                    inputStyle={styles.inputStyle}
                    disabled={true}
                    value={this.state[item.key]}
                  />
                </View>
              )
            } else {
              return (
                <View key={index}>
                  <Input 
                    placeholder={item.label}
                    inputContainerStyle={{ borderBottomColor: '#f0f1f2' }}
                    inputStyle={styles.inputStyle}
                    onChangeText={(val) => this.onChangeField(val, item.key)}
                    value={this.state[item.key]}
                  />
                </View>
              )
            }
          })}
          </View>
          <View style={styles.field}>
            {this.state.secondGroup.map((item, index) => {
              if (item.label === "Batch Id") {
                return (
                  <View key={index}>
                    <Input 
                      placeholder={item.label}
                      inputContainerStyle={{ borderBottomColor: '#f0f1f2' }}
                      inputStyle={styles.inputStyle}
                      disabled={true}
                      value={this.state[item.key]}
                    />
                  </View>
                )
              } else {
                return (
                  <View key={index}>
                    <Input 
                      placeholder={item.label}
                      inputContainerStyle={{ borderBottomColor: '#f0f1f2' }}
                      inputStyle={styles.inputStyle}
                      onChangeText={(val) => this.onChangeField(val, item.key)}
                      value={this.state[item.key]}
                    />
                  </View>
                )
              }
            })}
          </View>
      </View>
    )
  }

  render() {
    return (
      <ScrollView>
        <Card>
          {this.renderAccessorDetails()}
          {this.state.error && 
          (
            <View style={styles.errorContainer}>
              <Text style={{ color: 'red' }}>
                Please enter all the fields.
              </Text>
            </View>
          )
          }
          <Button
            title="Next"
            onPress={this.goToStep2}
          />
        </Card>
      </ScrollView>
    )
  }
}

const styles = {
  field: {
    flex: 0.5
  },
  inputStyle: {
    backgroundColor: '#f0f1f2', 
    borderColor: '#dedfe0',
    borderWidth: 1,
    fontSize: 16,
    paddingLeft: 25
  },
  errorContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10
  }
}

export default AnnexureForm1;