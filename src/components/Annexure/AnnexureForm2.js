import React, { Component } from 'react';
import { View, ScrollView, TouchableOpacity, Image, ActivityIndicator } from 'react-native';
import { Card, Text, Button, CheckBox, Input } from 'react-native-elements';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';

import { saveAnnexureDetails } from '../../actions/saveActions';
import AlertModal from '../../components/Common/AlertModal';

class AnnexureForm2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      saved: false,
      assessorDetails: [
        {"label": "Is the center located at the same address as mentioned on SDMS?"},
        {"label": "Is the Infrastructure (No. of classrooms, State of Laboratories, etc.) present at the Training Center adequate and satisfactory to conduct the training?"},
        {"label": "Is the assessment being conducted at same center, where trainees got trained?"},
        {"label": "Assessment is conducted on the Scheduled Date?"},
        {"label": "If No, Reasons for change of Date?"},
        {"label": "Are the Trainees being assessed for same job role, for which they were trained?"},
        {"label": "Each Trainee’s identity checked before conducting assessment?"},
        {"label": "Are the trainees confirming that they paid the Assessment Fee?"},
        {"label": "Are the trainees aware they are enrolled under PMKVY Scheme?"},
        {"label": "Are the trainees aware of the Certification Process?"},
        {"label": "Are the trainees aware of Reward Disbursement?"},
        {"label": "Are the trainees aware of the Course fee?"},
        {"label": "Are the trainees aware of Auto Debit Facility?"},
        {"label": "Are the trainees aware of the Placement opportunities?"},
        {"label": "Trainee Feedback form available, and signed by the trainee?"},
        {"label": "Trainee Attendance Record Available, and signed by the trainees?"},
        {"label": "Trainees Enrolment Forms available , with PMKVY logo and PMKVY Disclaimer? Are the forms signed by trainees?"},
        {"label": "Are the names on Enrolment forms tallying with the Attendance records of the batch (check any 5 forms)?"},
        {"label": "Are the trainees given a PMKVY 2 RPL booklet/ Pamphlet for information?"},
        {"label": "Posters of PMKVY 2 RPL displayed in Counseling room?"},
        {"label": "Posters of PMKVY 2 RPL displayed in Classroom?"},
        {"label": "PMKVY 2 RPL signage displayed outside the Center?"}
      ],
      checked1: { yes: false, no: false },
      checked2: { yes: false, no: false },
      checked3: { yes: false, no: false },
      checked4: { yes: false, no: false },
      checked5: { yes: false, no: false },
      checked6: { yes: false, no: false },
      checked7: { yes: false, no: false },
      checked8: { yes: false, no: false },
      checked9: { yes: false, no: false },
      checked10: { yes: false, no: false },
      checked11: { yes: false, no: false },
      checked12: { yes: false, no: false },
      checked12: { yes: false, no: false },
      checked13: { yes: false, no: false },
      checked14: { yes: false, no: false },
      checked15: { yes: false, no: false },
      checked16: { yes: false, no: false },
      checked17: { yes: false, no: false },
      checked18: { yes: false, no: false },
      checked19: { yes: false, no: false },
      checked20: { yes: false, no: false },
      checked21: { yes: false, no: false },
      checked22: { yes: false, no: false },
      comment_1: "",
      comment_2: "",
      comment_3: "",
      comment_4: "",
      comment_5: "",
      comment_6: "",
      comment_7: "",
      comment_8: "",
      comment_9: "",
      comment_10: "",
      comment_11: "",
      comment_12: "",
      comment_12: "",
      comment_13: "",
      comment_14: "",
      comment_15: "",
      comment_16: "",
      comment_17: "",
      comment_18: "",
      comment_19: "",
      comment_20: "",
      comment_21: "",
      comment_22: "",
      imagesUploaded: {
        image_1: false,
        image_2: false,
        image_3: false,
        image_4: false,
        image_5: false,
        image_6: false,
        image_7: false,
        image_8: false,
        image_9: false,
        image_10: false, 
        image_11: false, 
        image_12: false, 
        image_13: false, 
        image_14: false, 
        image_15: false, 
        image_16: false, 
        image_17: false, 
        image_18: false, 
        image_19: false, 
        image_20: false, 
        image_21: false, 
        image_22: false,      
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { uploadSerial, saved } = this.props;

    if (saved && saved !== prevProps.saved) {
      this.setState({ saved: true });
    }

    if (uploadSerial && uploadSerial != prevProps.uploadSerial) {
      const image = `image_${uploadSerial}`;
      this.setState({ imagesUploaded: { ...this.state.imagesUploaded, [image]: true } });
    }
  }

  onSelectOption = (val1, val2, i) => {
    const checked = `checked${i}`;
    this.setState(prevState => ({
      [checked]: {
        ...prevState.checked,
        [val1]: true,
        [val2]: false
      }
    }));
  }

  onChangeField = (value, fieldIndex) => {
    const field = `comment_${fieldIndex}`
    this.setState({ [field]: value });
  }

  renderUploadImage = (serial) => {
    const image = `image_${serial}`;
    if (this.state.imagesUploaded[image]) {
      return (
        <TouchableOpacity>
          <Image style={styles.uploadButton} source={require('../../../assets/img/check-yes.png')} />
        </TouchableOpacity>
      )
    } 
  
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate("Camera", { type: 'Annexure', serial })}>
        <Image style={styles.uploadButton} source={require('../../../assets/img/upload.png')} />
      </TouchableOpacity>
    )
  }

  renderAccessorDetails() {
    
    return this.state.assessorDetails.map((detail, i) => {
      const serial = i+1;
      const checked = `checked${serial}`;
      const comment = `comment_${serial}`;
      return (
        <View key={i} style={styles.form}>
          <View style={styles.fieldQuestion}>
            <Text>{i + 1}. {detail.label}</Text>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 0.2 }}>
              <CheckBox
                containerStyle={{ backgroundColor: "transparent", borderWidth: 0 }}
                title="Yes"
                checkedIcon='dot-circle-o'
                uncheckedIcon='circle-o'
                checked={this.state[checked].yes}
                onPress={() => this.onSelectOption('yes', 'no', serial)}
              />
            </View>
            <View style={{ flex: 0.2 }}>
              <CheckBox
                containerStyle={{ backgroundColor: "transparent", borderWidth: 0 }}
                title="No"
                checkedIcon='dot-circle-o'
                uncheckedIcon='circle-o'
                checked={this.state[checked].no}
                onPress={() => this.onSelectOption('no', 'yes', serial)}
              />
            </View>
            <View style={{ flex: 0.5 }}>
              <Input 
                placeholder="Comment"
                inputContainerStyle={{ borderBottomColor: '#f0f1f2' }}
                inputStyle={styles.inputStyle}
                onChangeText={(val) => this.onChangeField(val, serial)}
                value={this.state[comment]}
              />
            </View>
            <View style={{ flex: 0.1, marginLeft: 10 }}>
              {this.renderUploadImage(serial)}
            </View>
          </View>

        </View>
      )
    });
  }

  getBinaryFromBoolean = (data) => {
    const { yes } = data;

    if (yes == true) {
      return 1;
    } else {
      return 0;
    }
  }

  submit = () => {
    const { step1Details } = this.props;
    const { checked1, checked2, checked3, checked4, checked5, checked6, checked7, checked8, checked9, checked10, checked11, checked12, checked13, checked14, checked15, checked16, checked17, checked18, checked19, checked20, checked21, checked22 } = this.state;
    const { comment_1, comment_2, comment_3, comment_4, comment_5, comment_6, comment_7, comment_8, comment_9, comment_10, comment_11, comment_12, comment_13, comment_14, comment_15, comment_16, comment_17, comment_18, comment_19, comment_20, comment_21, comment_22 } = this.state;
    const step2Details = {
      question_1: [this.getBinaryFromBoolean(checked1), comment_1],
      question_2: [this.getBinaryFromBoolean(checked2), comment_2],
      question_3: [this.getBinaryFromBoolean(checked3), comment_3],
      question_4: [this.getBinaryFromBoolean(checked4), comment_4],
      question_5: [this.getBinaryFromBoolean(checked5), comment_5],
      question_6: [this.getBinaryFromBoolean(checked6), comment_6],
      question_7: [this.getBinaryFromBoolean(checked7), comment_7],
      question_8: [this.getBinaryFromBoolean(checked8), comment_8],
      question_9: [this.getBinaryFromBoolean(checked9), comment_9],
      question_10: [this.getBinaryFromBoolean(checked10), comment_10],
      question_11: [this.getBinaryFromBoolean(checked11), comment_11],
      question_12: [this.getBinaryFromBoolean(checked12), comment_12],
      question_13: [this.getBinaryFromBoolean(checked13), comment_13],
      question_14: [this.getBinaryFromBoolean(checked14), comment_14],
      question_15: [this.getBinaryFromBoolean(checked15), comment_15],
      question_16: [this.getBinaryFromBoolean(checked16), comment_16],
      question_17: [this.getBinaryFromBoolean(checked17), comment_17],
      question_18: [this.getBinaryFromBoolean(checked18), comment_18],
      question_19: [this.getBinaryFromBoolean(checked19), comment_19],
      question_20: [this.getBinaryFromBoolean(checked20), comment_20],
      question_21: [this.getBinaryFromBoolean(checked21), comment_21],
      question_22: [this.getBinaryFromBoolean(checked22), comment_22],
    }

    const annexureDetails = {
      step1Details,
      step2Details
    }

    this.props.saveAnnexureDetails(annexureDetails);
  }

  render() {
    return (
      <ScrollView>
        <Card>
          {this.renderAccessorDetails()}
          <Button
            title="Submit"
            onPress={this.submit}
          />
          {this.state.saved && 
            <AlertModal 
              type="One"
              label="SAVED" 
              message="Annexure Form Saved!"
              onPress={() => this.props.navigation.navigate("Home")} 
            />
          }
        </Card>
      </ScrollView>
    )
  }
}

const mapStateToProps = ({ save }) => {
  return {
    saved: save.annexureSaved,
    uploadSerial: save.uploadSerial
  }
}

export default connect(mapStateToProps, { saveAnnexureDetails })(withNavigation(AnnexureForm2));

const styles = {
  container: {
    marginTop: 10,
    marginBottom: 10
  },
  form: {
    borderColor: '#D3D3D3',
    elevation: 0.8,
    marginBottom: 20
  },
  fieldQuestion: {
    backgroundColor: "#c9e7f5",
    padding: 10,
    marginBottom: 20
  },
  inputStyle: {
    borderColor: '#dedfe0',
    borderWidth: 1,
    fontSize: 14,
    paddingLeft: 25
  },
  uploadButton: {
    width: 60,
    height: 50,
    resizeMode: 'contain'
  }
}