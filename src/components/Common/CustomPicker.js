import React from "react";
import { Picker } from 'react-native';

const CustomPicker = (props) => {
  return (
    <Picker
      selectedValue={props.selectedValue}
      style={{ height: 60, width: 250, ...props.style }}
      onValueChange={props.onChange}
    >
      {props.options.length > 0 ? props.options.map((option, i) => {
        if (props.placeholder === 'Select Language') {
          return (
            <Picker.Item label={option.language_name} value={option.id} key={i} />
          )
        } else if (props.placeholder === 'Select Marks') {
          return (
            <Picker.Item label={option.toString()} value={option.toString()} key={i} />
          )
        } else {
          return (
            <Picker.Item label={option.id} value={option.id} key={i} />
          )
        }
        
      }) : (
        <Picker.Item label={props.placeholder} value={"0"} />
      )}
    </Picker>
  )
}

export default CustomPicker;