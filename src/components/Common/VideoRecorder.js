import React, { Component } from 'react';
import { View, Dimensions } from 'react-native';
import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/FontAwesome';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

class VideoRecorder extends Component {
  state = {
    recording: false
  }

  captureVideo = async () => {
    if (this.camera) {
      const { recording } = this.state;

      if (recording) {
        this.camera.stopRecording();
        this.setState({ recording: false });
        return;
      }
      this.setState({ recording: true });

      const options = { quality: RNCamera.Constants.VideoQuality["4:3"], videoBitrate: 0.25*1000*1000 }
      const { uri } = await this.camera.recordAsync(options);

      this.props.saveStudentVideo(uri);
    }
  }

  render() {
    return (
      <View style={styles.videoContainer}>
        <RNCamera
          ref={(ref) => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          captureAudio={true}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        />
        <Icon name={this.state.recording ? "pause" : "play"} size={30} color="#fff" style={styles.icon} onPress={this.captureVideo} />
      </View>
    )
  }
}

export default VideoRecorder;

const styles = {
  videoContainer: {
    flex: 1,
    position: 'absolute',
    bottom: 0,
    right: 20,
  },
  preview: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    width: SCREEN_WIDTH / 3.8,
    height: SCREEN_HEIGHT / 4,
    zIndex: 99999999
  },
  icon: {
    top: -40,
    width: 50,
    zIndex: 999999999 
  }
}
