import React from "react";
import { View, Alert } from "react-native";

const AlertModal = (props) => {
  const createOneButtonAlert = () => {
    Alert.alert(
      props.label,
      props.message,
      [
        { text: "OK", onPress: props.onPress }
      ],
      { cancelable: false }
    );
  }

  const createTwoButtonAlert = () => {
    Alert.alert(
      props.label,
      props.message,
      [ 
        {
          text: "Cancel",
          onPress: props.onCancelPress
        },
        { text: "OK", onPress: props.onPress }
      ],
      { cancelable: false }
    );
  }

  return (
    <View style={styles.container}>
      {props.type === "One" ? createOneButtonAlert() : createTwoButtonAlert()}
    </View>
  );
}

const styles = {
  container: {
    flex: 1,
    justifyContent: "space-around",
    alignItems: "center"
  }
};

export default AlertModal;