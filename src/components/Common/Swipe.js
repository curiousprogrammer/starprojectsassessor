import React, { Component } from 'react';
import { View, Animated, PanResponder, Dimensions, LayoutAnimation, UIManager } from 'react-native';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const SWIPE_THRESHOLD = 0.3 * SCREEN_WIDTH;
const SWIPE_OUT_DURATION = 100;

class Swipe extends Component {
  static defaultProps = {
    onSwipeRight: () => {},
    onSwipeLeft: () => {},
  }

  constructor(props) {
    super(props);
    this.state = { 
      index: 0 
    };

    const position = new Animated.ValueXY();
    const panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gesture) => {
        position.setValue({ x: gesture.dx, y: 0 });  
      },
      onPanResponderRelease: (event, gesture) => {
        if (gesture.dx > SWIPE_THRESHOLD){
          this.forceSwipe('right');
        } else if (gesture.dx < -SWIPE_THRESHOLD) {
          this.forceSwipe('left');
        } else {
          this.resetPosition();
        }
      }
    });

    this.panResponder = panResponder;
    this.position = position; 
  }

  componentDidUpdate(prevProps, prevState) {
    UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
    LayoutAnimation.spring();

    if (prevProps.data !== this.props.data) {
      this.setState({ index: 0 });
    }

    if (prevProps.activeQuestion !== this.props.activeQuestion) {
      if (this.state.index !== this.props.activeQuestion) {
        this.setState({ index: this.props.activeQuestion });
      }
    }
  }

  forceSwipe(direction) {
    const x = direction === 'right' ? SCREEN_WIDTH : -SCREEN_WIDTH;
    Animated.timing(this.position, {
      toValue: { x, y: 0 },
      duration: SWIPE_OUT_DURATION,
      useNativeDriver: false
    }).start(() => this.onSwipeComplete(direction));
  }

  resetPosition() {
    Animated.spring(this.position, {
      toValue: { x: 0, y: 0 },
      useNativeDriver: false
    }).start();
  }

  onSwipeComplete(direction) {
    const { data } = this.props;

    this.position.setValue({ x: 0, y: 0 });
    
    if (direction === 'left') {
      this.setState({ index: this.state.index + 1 }, () => {
        this.onAfterComplete(direction);
      });
    } else if (direction === 'right') {
      this.setState({ index: this.state.index - 1 }, () => {
        this.onAfterComplete();
      });
    }
  }

  onAfterComplete(direction) {
    const { onSwipeLeft, onSwipeRight, data } = this.props;

    if (this.state.index === data.length) {
      this.setState({ index: 0 }, () => {
        const item = data[this.state.index];
        onSwipeLeft(item, this.state.index);
      });
    } else if (this.state.index < 0) {
      this.setState({ index: data.length - 1 }, () => {
        const item = data[this.state.index];
        onSwipeRight(item, this.state.index);
      });
    } else {
      const item = data[this.state.index];
      direction === 'left' ? onSwipeLeft(item, this.state.index) : onSwipeRight(item, this.state.index);
    }
  }

  getCardStyle() {
    const rotate = this.position.x.interpolate({
      inputRange: [-SCREEN_WIDTH * 1.5, 0, SCREEN_WIDTH * 1.5],
      outputRange: ['0deg', '0deg', '0deg']
    });

    return {
      ...this.position.getLayout(),
      transform: [{ rotate }]
    };
  }

  renderCards() {
    return this.props.data.map((item, i) => {
      if (i < this.state.index) { 
        return (
          <Animated.View
            key={i} 
            pointerEvents="none"
            style={[styles.cardStyle, { opacity: 0 }]}
            {...this.panResponder.panHandlers}
          >
            {this.props.renderCard(item, i)}
          </Animated.View>
        )
      }

      if (i === this.state.index) {
        return (
          <Animated.View
            key={i}
            style={[this.getCardStyle(), styles.cardStyle]}
            {...this.panResponder.panHandlers}
          >
            {this.props.renderCard(item, i)}
          </Animated.View>
        );
      }

      return (
        <Animated.View 
          key={i} 
          pointerEvents="none"
          style={[styles.cardStyle, { opacity: 0 }]}
          {...this.panResponder.panHandlers}
        >
          {this.props.renderCard(item, i)}
        </Animated.View>
      )
    });
  }

  render() {
    return(
      <View style={styles.cardsContainer}>
        {this.renderCards()}
      </View>
    );
  }
}

const styles = {
  cardsContainer: {
    height: SCREEN_HEIGHT / 1.32
  },
  cardStyle: {
    position: 'absolute',
    width: SCREEN_WIDTH / 1.5,
    elevation: 1
  }
};

export default Swipe;