import  React, { Component } from 'react';
import { Text, View } from 'react-native';
import BackgroundTimer from 'react-native-background-timer';

class Timer extends Component {
  constructor(props) {
    super(props);

    let { duration } = this.props;
    duration = parseInt(duration);
    const convertedTime = this.timeConvert(duration);
    
    this.state = {
      duration: duration ? duration : null,
      start: false,
      hour: convertedTime.hours,
      min: convertedTime.minutes,
      sec: convertedTime.seconds,
    }

    this.interval = null;
  }

  componentDidMount() {
    this.setState({ start: true }, () => {
      this.handleStart();
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.duration && this.state.hour === 0 && this.state.min === 0 && this.state.sec === 0) {
      this.props.endTest();
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  timeConvert(duration) {
    if (duration) {
      const hours = (duration / 60);
      const rhours = Math.floor(hours);
      const minutes = (hours - rhours) * 60;
      const rminutes = Math.round(minutes);
      if (rhours > 0) {
        return { hours: rhours - 1, minutes: 59, seconds: 60 };
      } else if (rminutes > 0) {
        return { hours: rhours, minutes: rminutes - 1, seconds: 60 };
      } else {
        return { hours: rhours, minutes: rminutes, seconds: 0 };
      }
    }
    return { hours: 0, minutes: 0, seconds: 0 };
  }

  handleStart = () => {
    if (this.state.start) {
      this.interval = BackgroundTimer.setInterval(() => {
        if (this.state.duration) {
          if (this.state.sec !== 0) {
            this.setState({
              sec: --this.state.sec
            });
          } else if (this.state.min !== 0) {
            this.setState({
              sec: 59,
              min: --this.state.min
            });
          } else if (this.state.hour !== 0) {
            this.setState({
              sec: 59,
              min: 59,
              hour: this.state.hour - 1
            });
          }
        } else {
          if (this.state.sec !== 59) {
            this.setState({
              sec: ++this.state.sec
            });
          } else if (this.state.min !== 59) {
            this.setState({
              sec: 0,
              min: ++this.state.min
            });
          } else {
            this.setState({
              sec: 0,
              min: 0,
              hour: this.state.hour + 1
            });
          }
        }
      }, 1000);
    } else {
      clearInterval(this.interval);
    }
  };

  handleReset = () => {
    this.setState({
      hour: 0,
      min: 0,
      sec: 0,
      start: false
    });
    clearInterval(this.interval);
  };

  padToTwo = (number) => {
    return number <= 9 ? `0${number}`: number;
  };

  render() {
    return (
      <View style={styles.timerContainer}>
        <Text style={styles.timerText}>{this.padToTwo(this.state.hour) + ' : '}</Text>
        <Text style={styles.timerText}>{this.padToTwo(this.state.min) + ' : '}</Text>
        <Text style={styles.timerText}>{this.padToTwo(this.state.sec)}</Text>
      </View>
    )
  }
}

const styles = {
  timerContainer: {
    flexDirection: 'row',
    marginLeft: 30
  },
  timerText: {
    fontSize: 18,
    color: "#FFF",
  }
}

export default Timer;