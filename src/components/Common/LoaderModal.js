import React from 'react';
import { ActivityIndicator, Text, View, Dimensions } from 'react-native';
import Modal from 'react-native-modal';

const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;

const LoaderModal = (props) => {
  return (
    <Modal 
      isVisible={props.isModalVisible} 
      deviceWidth={deviceWidth}
      deviceHeight={deviceHeight}
    >
      <View style={{ flex: 1, marginTop: 50 }}>
        <Text style={styles.loaderText}>Please wait while data is being uploaded!</Text>
        <ActivityIndicator size="large" color="#fff" />
      </View>
    </Modal>
  );
}

export default LoaderModal;

const styles = {
  loaderContainer: {
    marginTop: 10,
    marginBottom: 10
  },
  loaderText: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 22
  }
}