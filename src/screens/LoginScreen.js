import React, { Component } from 'react';
import { View, Image } from 'react-native';
import SplashScreen from 'react-native-splash-screen';

import Card from '../components/Common/Card';
import AssessorLoginForm from '../components/Login/AssessorLoginForm';
import globalStyles from '../common/globalStyles';

class LoginScreen extends Component {
  componentDidMount() {
    SplashScreen.hide();
  }

  changeLoginType = (loginType) => {
    this.setState({ loginType });
  }

  goToVerify = () => {
    this.props.navigation.navigate("Verification");
  }

  render() {
    return (
      <View style={globalStyles.container}>
        <Card style={{ marginTop: 50 }}>
          <View style={styles.logoContainer}>
            <Image
              style={styles.logo}
              source={require('../../assets/img/logo.png')}
            />
          </View>
          <View style={{ alignItems: 'center' }}>
            <AssessorLoginForm 
              goToVerify={this.goToVerify}
            />
          </View>
        </Card>
      </View>

    )
  }
}

const styles = {
  logoContainer: {
    alignItems: 'center'
  },
  logo: {
    width: 400,
    height: 250,
    resizeMode: 'contain'
  },
  button: {
    backgroundColor: '#2975a0',
    borderRadius: 20,
    paddingTop: 15,
    paddingBottom: 15
  }
}

export default LoginScreen;