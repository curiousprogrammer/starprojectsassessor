import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';

import gs from '../common/globalStyles';
import { getAssessmentData } from '../common/utils';
import { saveAssessmentDetails } from '../actions/testActions';
import { resetSavedImage } from '../actions/saveActions';

class TestInfoScreen extends Component {
  constructor(props) {
    super(props);

    const { details } = this.props.navigation.state.params;

    this.state = {
      centerID: details.centerID,
      candidateID: details.candidateID,
      assessmentID: details.assessmentID,
      details,
      testData: null,
      savedImage: false
    }
  }
  
  async componentDidMount() {
    const { centerID, assessmentID } = this.state;

    try {
      const testData = await getAssessmentData('assessmentData', centerID, assessmentID);
      this.setState({ testData: testData });

      const assessmentDetails = {
        centerID,
        assessmentID,
        assessmentName: testData.assessment.assessment_name,
        assessmentDuration: testData.assessment.assessment_duration
      }

      this.props.saveAssessmentDetails(assessmentDetails);

    } catch (e) {
      console.log(e);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { savedImage } = this.props;

    if (savedImage && savedImage !== prevProps.savedImage) {
      this.setState({ savedImage: true });
    }
  }

  componentWillUnmount() {
    this.props.resetSavedImage();
  }

  startAssessment = () => {
    this.props.navigation.navigate("Test", { 
      testData: this.state.testData, 
      details: this.state.details
    });

    this.props.resetSavedImage();
  }

  render() {
    return (
      <View style={gs.container}>
        <View style={{ marginTop: 50 }}>
          <Text style={styles.header}>
            About Submission:
          </Text>
        </View>
        <View style={{ marginTop: 50 }}>
          <Text style={styles.instructions}>
            1. Assessor must take photographs of the candidate with original ID either in group or individual.
          </Text>
          <Text style={styles.instructions}>
            2. Assessor must ensure that each trainee perform each steps of each NOS in Skill (Practical) Assessment and fill their corresponding marks against each step very attentively.
          </Text>
          <Text style={styles.instructions}>
            3. Assessor must save the filled marks provided in each step after completing every task and move to next task.
          </Text>
          <Text style={styles.instructions}>
            4. After completion of all the task of QP click on submit button and save the assessment.
          </Text>
          <Text style={styles.instructions}>
            5. Assessor must instruct the candidates that the marking, will also depend upon productivity and quality of performed task.
          </Text>
          <Text style={styles.instructions}>
            6. Assessor must ensure to take photographic and video graphic evidences of each candidates for each NOS.
          </Text>
        </View>
        <View style={{ marginTop: 40 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <Button 
              title="Capture Photo"
              containerStyle={{ marginRight: 100 }}
              buttonStyle={styles.button}
              onPress={() => this.props.navigation.navigate("Camera", 
              { 
                type: 'Student', 
                candidateID: this.state.candidateID,
                assessmentID: this.state.assessmentID
              }
              )}
            />
            <Button
              title="Start Assessment"
              buttonStyle={styles.button}
              disabled={!this.state.savedImage}
              onPress={this.startAssessment}
            />
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = ({ save }) => {
  return {
    savedImage: save.savedImage
  }
}

export default connect(mapStateToProps, { saveAssessmentDetails, resetSavedImage })(TestInfoScreen);

const styles = {
  button: {
    height: 50,
    width: 300
  },
  header: {
    alignSelf: 'center',
    fontWeight: 'bold',
    fontSize: 26
  },
  instructions: {
    fontSize: 18,
    marginBottom: 20
  }
}