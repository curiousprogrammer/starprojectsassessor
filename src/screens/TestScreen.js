import React, { Component } from 'react';
import { View, Text, TextInput, FlatList, TouchableOpacity, SafeAreaView, Dimensions, BackHandler, Image } from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';

import { savePracticalAnswers, saveStudentVideo, resetTestSaved, saveCompletedStudent } from '../actions/saveActions';
import globalStyles from '../common/globalStyles';
import Header from '../components/Common/Header';
import CustomPicker from '../components/Common/CustomPicker';
import Timer from '../components/Common/Timer';
import Card from '../components/Common/Card';
import Swipe from '../components/Common/Swipe';
import AlertModal from '../components/Common/AlertModal';
import VideoRecorder from '../components/Common/VideoRecorder';

class TestScreen extends Component {

  constructor(props) {
    super(props);

    const { testData: { assessment }, details } = this.props.navigation.state.params;
    const languages = assessment.language;
    const practical_question = assessment.practical_question;
    const numberOfQuestions = practical_question.length;

    this.state = {
      details,
      assessmentData: practical_question,
      selectedLanguage: languages[0].id,
      selectedLanguageName: languages[0].language_name,
      languages,
      activeQuestion: 0,
      numberOfQuestions,
      testSaved: false,
      videoButtonLabel: "Open Camera",
      cameraVisible: false,
      endConfirm: false,
      textShown: 0
    }

  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    this.props.resetTestSaved();
  }

  componentDidUpdate(prevProps, prevState) {
    const { testSaved } = this.props;

    if (testSaved && testSaved != prevProps.testSaved) {
      this.setState({ testSaved });
    }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  handleBackButton() {
    return true;
  }

  onChangeLanguage = (itemValue, itemIndex) => {
    this.setState({ selectedLanguage: itemValue });

    const { languages } = this.state;

    let changedLanguage;
    languages.forEach((language) => {
      if (language.id === itemValue){
        changedLanguage = language.language_name;
      } 
    });

    this.setState({ 
      selectedLanguage: itemValue,
      selectedLanguageName: changedLanguage
    });
  }

  toggleCamera = () => {
    const { cameraVisible } = this.state;

    let videoButtonLabel;
    if (!cameraVisible) {
      videoButtonLabel = "Close Camera";
    } else {
      videoButtonLabel = "Open Camera";
    }

    this.setState({ cameraVisible: !cameraVisible, videoButtonLabel });
  }

  onSaveStudentVideo = (uri) => {
    const { details: { candidateID, assessmentID, assessorID } } = this.state;

    this.props.saveStudentVideo(uri, candidateID, assessmentID, assessorID);
  }

  onMarksEntered = (questionIndex, marksIndex, marks) => {
    const assessmentDataCopy = this.state.assessmentData;
    if (parseInt(marks) <= parseInt(assessmentDataCopy[questionIndex].steps[marksIndex].totalMarks)) {
      assessmentDataCopy[questionIndex].steps[marksIndex].marksReceived = marks;
    } else if (marks === "") {
      assessmentDataCopy[questionIndex].steps[marksIndex].marksReceived = marks;
    }
    this.setState({ assessmentData: assessmentDataCopy });
  }

  onSaveQuestion = () => {
    const { activeQuestion, assessmentData } = this.state;
    const assessmentDataCopy = assessmentData;

    assessmentDataCopy[activeQuestion].saved = true;
  
    this.setState({ assessmentData: assessmentDataCopy });
  }

  onSkipQuestion = () => {
    const { activeQuestion, numberOfQuestions } = this.state;

    let newActiveQuestion = activeQuestion;

    if (activeQuestion < numberOfQuestions - 1) {
      newActiveQuestion++;
    } else {
      newActiveQuestion = 0;
    }

    this.setState({ activeQuestion: newActiveQuestion });
  }

  onSubmit = () => {
    this.setState({ endConfirm: false });
    const { assessmentData, details: { candidateID, assessmentID, centerID }  } = this.state;

    const answersObj = assessmentData.map((question) => {
      return {
        question_id: question.question_id,
        marks: question.steps.map((step) => ({ marks: step.marksReceived }))
      }
    });

    this.props.savePracticalAnswers(answersObj, candidateID, assessmentID);
    this.props.saveCompletedStudent(candidateID, centerID);
  }

  toggleNumberOfLines = index => {
    this.setState({
      textShown: this.state.textShown === index ? -1 : index,
    });
  };

  renderCard = (item, index) => {
    return (
      <Card style={styles.testCard}>
        <View key={item.u_question_id}>
          <View style={styles.nosContainer}>
            <Text style={styles.nos}>NOS: {item.nos_name}</Text>
          </View>
          <Text style={styles.question}>{index + 1}. {item.question[this.state.selectedLanguageName]}</Text>
          {item.question_image !== "" && (
            <Image style={styles.questionImage} source={{ uri: item.question_image }} resizeMode="contain" />
          )}
          {item.steps.map(({ totalMarks, marksReceived, steps }, i) => {
            if (steps[this.state.selectedLanguageName]) {
              return (
                <View key={i}>
                  <View>
                    <Text 
                      style={styles.step}
                      numberOfLines={this.state.textShown === i ? undefined : 1}
                    >
                      {i + 1}. {steps[this.state.selectedLanguageName]}
                    </Text>
                    <Text
                      onPress={() => this.toggleNumberOfLines(i)}
                      style={{ color: 'blue' }}>
                      {this.state.textShown === i ? 'read less...' : 'read more...'}
                    </Text>
                  </View>
                  <View style={styles.marksFieldContainer}>
                    <TextInput
                      placeholder="Enter Marks"
                      style={{ height: 40, width: 50, borderBottomWidth: 1 }}
                      keyboardType='number-pad'
                      onChangeText={(value) => this.onMarksEntered(index, i, value)}
                      value={marksReceived}
                      maxLength={3}
                    />
                    <Text style={{ paddingTop: 10 }}> / {totalMarks} marks</Text>
                  </View>
                </View>
              )
            }
          })}
        </View>
        {!item.saved ?
          (
            <View style={styles.buttonContainer}>
              <Button
                title="Save"
                buttonStyle={styles.button}
                containerStyle={{ marginRight: 20 }}
                onPress={this.onSaveQuestion}
              />
              <Button
                title="Skip"
                buttonStyle={[styles.button, { backgroundColor: 'black' }]}
                onPress={this.onSkipQuestion}
              />
            </View>
          ) : (
            <View>
              <Button
                title="Next"
                buttonStyle={[styles.button, { backgroundColor: 'black' }]}
                onPress={this.onSkipQuestion}
              />
            </View>
          )
        }
      </Card>
    )
  }

  render() {
    const { activeQuestion, assessmentData } = this.state;
    return (
      <View style={styles.container}>
        {this.state.cameraVisible && <VideoRecorder saveStudentVideo={this.onSaveStudentVideo} />}
        <View style={{ flex: 0.5 }}>
          <Header headerTitle="All questions are mandatory">
            <View style={globalStyles.spaceBetween}>
              <View style={styles.pickerContainer}>
                <CustomPicker 
                  selectedValue={this.state.selectedLanguage}
                  options={this.state.languages}
                  placeholder="Select Language"
                  onChange={this.onChangeLanguage}
                  style={styles.pickerStyle}
                />
              </View>
              <View style={{ top: 25, flexDirection: 'row' }}>
                <Timer />
              </View>
            </View>
          </Header>

          {assessmentData && assessmentData.length > 0 ?
            (
              <View style={{ flexDirection: 'row', marginTop: 20, paddingHorizontal: 20 }}>
                <View style={{ flex: 0.7  }}>
                  <Swipe 
                    data={assessmentData}
                    activeQuestion={activeQuestion}
                    renderCard={(item, i) => this.renderCard(item, i)}
                    onSwipeLeft={(ques, i) => this.setState({ activeQuestion: i })}
                    onSwipeRight={(ques, i) => this.setState({ activeQuestion: i })}
                  />
                  <Button 
                    title={this.state.videoButtonLabel}
                    buttonStyle={styles.submitButton}
                    onPress={this.toggleCamera}
                  />
                  <Button 
                    title="SUBMIT"
                    buttonStyle={[styles.submitButton, { backgroundColor: '#32cd32' }]}
                    onPress={() => this.setState({ endConfirm: true })}
                  />
                </View>
                <View style={{ flex: 0.3, height: Dimensions.get('window').height }}>
                  <SafeAreaView>
                    <FlatList
                      contentContainerStyle={{ paddingBottom: 150 }}
                      data={assessmentData}
                      renderItem={({ item, index }) => 
                        (
                          <TouchableOpacity 
                            style={[
                              styles.navigation, 
                              index === activeQuestion && { backgroundColor: '#1D1D1D' },
                              item.saved && { backgroundColor: '#32cd32' }
                            ]} 
                            onPress={() => this.setState({ activeQuestion: index, textShown: 0 })}
                          >
                            <Text style={styles.navigationText}>Question {index+1}</Text>
                          </TouchableOpacity>
                        )
                      }
                      keyExtractor={item => item.u_question_id}
                    />
                  </SafeAreaView>
                </View>
              </View>
            ) : (
              <View>
                <Text style={styles.noQuestions}>No questions available.</Text>
              </View>
            )
          }

          {this.state.testSaved && 
            <AlertModal 
              type="One"
              label="Success" 
              message="Test Completed!"
              onPress={() => {
                this.props.navigation.navigate("StudentList");
              }} 
            />
          }

          {this.state.endConfirm && 
            <AlertModal 
              type="Two"
              label="Confirmation" 
              message="Are you sure?"
              onPress={this.onSubmit} 
              onCancelPress={() => this.setState({ endConfirm: false })}
            />
          }
        </View>
      </View>
    )
  }
}

const mapStateToProps = ({ save }) => {
  return {
    testSaved: save.testSaved
  }
}

export default connect(mapStateToProps, { savePracticalAnswers, saveStudentVideo, resetTestSaved, saveCompletedStudent })(TestScreen);

const styles = {
  preview: {
    alignItems: 'center',
    position: 'absolute',
    justifyContent: 'flex-end',
    width: 200,
    height: 300,
    bottom: 30,
    right: 30,
    zIndex: 99999999
  },
  container: {
    flex: 1
  },
  pickerContainer: {
    backgroundColor: '#c7c7c7',
    marginTop: 20,
    width: 115
  },
  pickerStyle: {
    height: 25,
    width: 125,
  },
  marksFieldContainer: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
  },
  timer: {
    color: '#fff'
  },
  testCard: {
    backgroundColor: '#fff',
  },
  nosContainer: {
    flexDirection:'row',
    flexWrap: 'wrap'
  },
  nos: {
    paddingHorizontal: 5,
    paddingVertical: 5,
    borderWidth: 1,
    borderRadius: 10,
    marginBottom: 10
  },
  question: {
    fontSize: 22
  },
  questionImage: {
    height: 200,
    width: 300,
    alignSelf: 'center'
  },
  step: {
    marginTop: 10,
    fontSize: 16
  },
  button: {
    width: 100
  },
  submitButton: {
    borderRadius: 10,
    marginBottom: 15,
    paddingHorizontal: 20,
    paddingVertical: 15,
    width: Dimensions.get('window').width / 1.5
  },
  buttonContainer: { 
    flexDirection: 'row', 
    marginTop: 20
  },
  navigation: {
    backgroundColor: '#2975a0',
    borderRadius: 10,
    height: 35,
    marginBottom: 15,
    marginLeft: 20,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 20,
    paddingRight: 10
  },
  navigationText: {
    color: 'white',
    fontSize: 16
  },
  noQuestions: {
    fontSize: 18, 
    marginTop: 20, 
    alignSelf: 'center'
  }
}