import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { Button, Header } from 'react-native-elements';
import { connect } from 'react-redux';

import UploadAnswers from './Upload/UploadAnswers';
import UploadVideos from './Upload/UploadVideos';
import UploadImages from './Upload/UploadImages';
import UploadAnnexureImages from './Upload/UploadAnnexureImages';
import AlertModal from '../../components/Common/AlertModal';
import LoaderModal from '../../components/Common/LoaderModal';
import { getData, getStudents, completeAssessment } from '../../common/localSave';
import  { uploadPracticalAnswers, uploadStudentImage, uploadStudentVideo, uploadAnnexureImage, uploadAnnexure } from '../../actions/uploadActions';
import globalStyles from '../../common/globalStyles';

class UploadDataList extends Component {
  constructor(props) {
    super(props);

    const { centerID, assessmentID } = this.props.navigation.state.params;

    this.state = {
      centerID,
      assessmentID,
      isVisible: false,
      isAnswersVisible: false,
      isVideosVisible: false,
      isImagesVisible: false,
      isAttendanceVisible: false,
      isAnnexureImageVisible: false,
      showAlert: false,
      assessment: "",
      studentList: [],
      uploadAttendance: 0,
      answers: {},
      uploadAnswers: 0,
      videos: [],
      uploadVideos: 0,
      images: [],
      uploadImages: 0,
      uploadError: false,
      annexureImages: [],
      uploadAnnexureImage: 0,
      annexureDetails: ""
    } 
  }

  async componentDidMount() {
    this.focusListener = this.props.navigation.addListener('willFocus', this.load);
  }

  load = async () => {
    const { centerID, assessmentID } = this.state;

    const assessmentList = await getData('@upload_data');
    const annexureImages = await getData('@annexure_images');
    const annexureDetails = await getData('@annexure_details');
    const students = await getStudents(centerID);

    if (assessmentList) {
      const assessment = assessmentList.find((assessmentData) => centerID == assessmentData.assessmentDetails.centerID && assessmentID == assessmentData.assessmentDetails.assessmentID);
      if (assessment) {
        if (!assessment.assessmentDetails.uploaded) {
          const { assessmentData: { answers, videos, images } } = assessment;
          this.setState({ assessment, answers: Object.values(answers), videos, images, studentList: students, annexureImages, annexureDetails });
        } else {
          this.setState({ assessment });
        }
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { answerUploaded, videoUploaded, imageUploaded, uploadError, annexureImageUploaded } = this.props;

    if (answerUploaded && answerUploaded != prevProps.answerUploaded) {
      this.setState({ uploadAnswers: ++this.state.uploadAnswers });
    }

    if (imageUploaded && imageUploaded != prevProps.imageUploaded) {
      this.setState({ uploadImages: ++this.state.uploadImages });
    }

    if (videoUploaded && videoUploaded != prevProps.videoUploaded) {
      this.setState({ uploadVideos: ++this.state.uploadVideos });
    }

    if (annexureImageUploaded && annexureImageUploaded != prevProps.annexureImageUploaded) {
      this.setState({ uploadAnnexureImage: ++this.state.uploadAnnexureImage });
    }

    if (uploadError && uploadError != prevProps.uploadError) {
      this.setState({ uploadError });
    }
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  showUploadData = () => {
    const { assessment } = this.state;

    if (!assessment.assessmentDetails.uploaded) {
      this.setState({ isVisible: !this.state.isVisible });
    }
  }

  uploadAnswer = (item) => {
    this.props.uploadPracticalAnswers(item.answers, item.candidate_id, item.assessment_id);
  }

  uploadVideo = (item) => {
    this.props.uploadStudentVideo(item);
  }

  uploadImage = (item) => {
    this.props.uploadStudentImage(item);
  }

  uploadAnnexureImage = (item) => {
    this.props.uploadAnnexureImage(item, this.state.assessmentID);
  }

  uploadAnnexureDetails = () => {
    const { annexureDetails } = this.state;
    if (annexureDetails) {
      this.props.uploadAnnexure(annexureDetails.step1Details, annexureDetails.step2Details);
    }
  }

  completeUpload = () => {
    const { assessment } = this.state;
    completeAssessment(assessment.assessmentDetails);

    const assessmentCopy = assessment;
    assessmentCopy.assessmentDetails.uploaded = true;

    this.setState({ assessmentList: assessmentCopy, showAlert: false, isVisible: false });
  }

  render() {
    const { assessment, isAnswersVisible, isVideosVisible, isImagesVisible, isAnnexureImageVisible } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <Header 
          centerComponent={{ text: 'Upload Data', style: { color: '#fff', fontSize: 18 } }}
          containerStyle={{ paddingTop: 20, height: 80 }}
        />
        <ScrollView style={globalStyles.container}>
          {assessment ?
            (
              <View>
                <TouchableOpacity 
                  style={styles.centerContainer}
                  onPress={() => this.showUploadData(assessment.assessmentDetails.centerID)}
                >
                  <View>
                    <View>
                      <View style={{ flexDirection: 'row'}}>
                        <Text style={styles.centerDetails}>Center ID: {assessment.assessmentDetails.centerID}</Text>
                        <Text style={[styles.statusText, assessment.assessmentDetails.uploaded ? { backgroundColor: 'limegreen' } : { backgroundColor: 'red' }]}>
                          {assessment.assessmentDetails.uploaded ? "Uploaded" : "Not Uploaded"}
                        </Text>
                      </View>
                      <Text style={styles.centerSubDetails}>Assessment Name: {assessment.assessmentDetails.assessmentName}</Text>
                      <Text style={styles.centerSubDetails}>Assessment Duration: {assessment.assessmentDetails.assessmentDuration} minutes</Text>
                    </View>
                  </View>
                </TouchableOpacity>
                  {this.state.isVisible && 
                    (
                      <View style={[styles.centerContainer, { marginBottom: 50 }]}>
                        <View>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10 }}>
                            <Text>Answers: {Object.keys(this.state.answers).length}</Text>
                            <Button 
                              buttonStyle={styles.button}
                              title="Upload Answers"
                              onPress={() => this.setState({ isAnswersVisible: !this.state.isAnswersVisible })}
                            />
                          </View>
                          {isAnswersVisible && <UploadAnswers 
                            answers={this.state.answers} 
                            uploadAnswer={this.uploadAnswer}
                          />}
                        </View>
                        <View>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10 }}>
                            <Text>Videos: {this.state.videos && this.state.videos.length}</Text>
                            <Button 
                              buttonStyle={styles.button}
                              title="Upload Videos"
                              onPress={() => this.setState({ isVideosVisible: !this.state.isVideosVisible })}
                            />
                          </View>
                          {isVideosVisible && <UploadVideos 
                            videos={this.state.videos} 
                            uploadVideo={this.uploadVideo}
                          />}
                        </View>
                        <View>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10 }}>
                            <Text>Images: {this.state.images && this.state.images.length}</Text>
                            <Button 
                              buttonStyle={styles.button}
                              title="Upload Images"
                              onPress={() => this.setState({ isImagesVisible: !this.state.isImagesVisible })}
                            />
                          </View>
                          {isImagesVisible && <UploadImages
                            images={this.state.images}
                            uploadImage={this.uploadImage}
                          />}
                        </View>
                        <View>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10 }}>
                            <Text>Annexure Images: {this.state.annexureImages && this.state.annexureImages.length}</Text>
                            <Button 
                              buttonStyle={styles.button}
                              title="Upload Annexure Images"
                              onPress={() => this.setState({ isAnnexureImageVisible: !this.state.isAnnexureImageVisible })}
                            />
                          </View>
                          {isAnnexureImageVisible && <UploadAnnexureImages
                            images={this.state.annexureImages}
                            uploadAnnexureImage={this.uploadAnnexureImage}
                          />}
                        </View>
                        <View>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10 }}>
                            <Text>Annexure Details</Text>
                            <Button 
                              buttonStyle={styles.button}
                              title="Upload Annexure Details"
                              onPress={this.uploadAnnexureDetails}
                            />
                          </View>
                        </View>
                        <Button 
                          title="Complete"
                          containerStyle={{ marginTop: 20 }}
                          buttonStyle={styles.completeButton}
                          onPress={() => this.setState({ showAlert: true })}
                        />
                        {this.props.loading && <LoaderModal isModalVisible={true} />}
                        {this.state.showAlert && 
                          <AlertModal 
                            type="Two"
                            label="Complete and delete all data?" 
                            message="Please make sure to upload all data before continuing with this step! Do you wish to continue?"
                            onPress={this.completeUpload}
                            onCancelPress={() => this.setState({ showAlert: false })}
                          />
                        }
                        {this.state.uploadError && 
                          <AlertModal 
                            type="One"
                            label="Error" 
                            message="Please try again!"
                            onPress={() => this.setState({ uploadError: false })}
                          />
                        }
                      </View>
                    )
                  }
              </View>
            ) : (
              <View>
                <Text style={styles.emptyText}>DATA NOT FOUND!</Text>
              </View>
            )
          }
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = ({ upload }) => {
  return {
    loading: upload.loading,
    uploadError: upload.uploadError,
    answerUploaded: upload.answerUploaded,
    imageUploaded: upload.imageUploaded,
    videoUploaded: upload.videoUploaded,
    annexureImageUploaded: upload.annexureImageUploaded
  }
}

export default connect(mapStateToProps, { uploadPracticalAnswers, uploadStudentImage, uploadStudentVideo, uploadAnnexureImage, uploadAnnexure })(UploadDataList);

const styles = {
  centerContainer: {  
    borderColor: '#D3D3D3',
    borderWidth : 1,
    paddingHorizontal: 15,
    paddingBottom: 15,
    marginVertical: 5
  },
  centerDetails: {
    fontSize: 18,
    marginTop: 15
  },
  centerSubDetails: {
    color: '#919191',
    fontSize: 16
  },
  emptyText: {
    alignSelf: 'center',
    fontSize: 20,
    marginTop: 50
  },
  statusText: {
    color: '#fff',
    paddingHorizontal: 5,
    paddingVertical: 5,
    marginLeft: 10,
    marginTop: 10
  },
  button: {
    width: 120
  },
  completeButton: {
    backgroundColor: 'limegreen',
    borderRadius: 20,
    paddingTop: 15,
    paddingBottom: 15
  }
}