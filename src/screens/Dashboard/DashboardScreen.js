import React from 'react';
import { View, Image, Text } from 'react-native';

import globalStyles from '../../common/globalStyles';

const DashboardScreen = () => {
  return (
    <View style={globalStyles.container}>
      <View style={styles.logoContainer}>
        <Image
          style={styles.logo}
          source={require('../../../assets/img/logo.png')}
        />
      </View>
      <View style={{ marginTop: 50 }}>
        <Text style={styles.headerText}>STAR Projects Services (P) Ltd.</Text>
        <View style={{ marginTop: 20 }}>
          <Text style={styles.descriptionText}>K-108 A, First Floor, Above Agarwal Sweet, Thokar No 5, Abul Fazal Enclave Part - 1, Jamia Nagar, Okhla, New Delhi - 110025</Text>
          <Text style={styles.descriptionText}>
            <Text style={{ fontWeight: 'bold' }}>Email: </Text>
            <Text>starprojectsservices@gmail.com</Text>
          </Text>
          <Text style={styles.descriptionText}>
            <Text style={{ fontWeight: 'bold' }}>Tele/Fax: </Text>
            <Text>011-26989501</Text>
          </Text>
          <Text style={styles.descriptionText}>
            <Text style={{ fontWeight: 'bold' }}>Mobile: </Text>
            <Text>+91-9811376288</Text>
          </Text>
        </View>
      </View>
    </View>
  )
}

export default DashboardScreen;

const styles = {
  headerText: {
    fontSize: 24,
    color: '#1b2775',
    alignSelf: 'center'
  },
  logoContainer: {
    alignItems: 'center'
  },
  logo: {
    width: 400,
    height: 250,
    resizeMode: 'contain'
  },
  descriptionText: {
    fontSize: 16,
    marginBottom: 15,
    alignSelf: 'center',
    textAlign: 'center',
  },
  buttonContainer: {
    marginBottom: 20
  },
  button: {
    backgroundColor: '#2975a0',
    borderRadius: 10,
    fontSize: 16,
    paddingVertical: 20,
  }
}