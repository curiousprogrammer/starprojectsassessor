import React from 'react';
import { View, Text, FlatList } from 'react-native';
import { Button } from 'react-native-elements';

const UploadAnnexureImages = (props) => {
  return (
    <FlatList 
      contentContainerStyle={{ paddingBottom: 30 }}
      data={props.images}
      renderItem={({ item, index }) => 
        (
          <View style={{ flexDirection: 'row', marginBottom: 15 }}>
            <Text style={styles.text}>{index + 1}.</Text>
            <Button 
              title="Upload"
              onPress={() => props.uploadAnnexureImage(item)}
            />
          </View>
        )
      }
      keyExtractor={item => item.serial}
    />
  )
}

export default UploadAnnexureImages;

const styles = {
  text: {
    marginRight: 20,
    paddingTop: 10
  }
}