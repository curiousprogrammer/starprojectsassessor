import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Header } from 'react-native-elements';

import { getData } from '../../common/localSave';
import AnnexureForm1 from '../../components/Annexure/AnnexureForm1';
import AnnexureForm2 from '../../components/Annexure/AnnexureForm2';

class AnnexureDetailsScreen extends Component {
  constructor(props) {
    super(props);

    const { centerID, batchID } = this.props.navigation.state.params;

    this.state = {
      centerID,
      batchID,
      isAnnexurePresent: false,
      isStep2: false,
      step1Details: ""
    }
  }

  async componentDidMount() {
    this.focusListener = this.props.navigation.addListener('willFocus', this.load);
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  load = async () => {
    try {
      const annexureDetails = await getData('@annexure_details');

      if (Object.keys(annexureDetails).length > 0 && typeof annexureDetails === 'object') {
        this.setState({ isAnnexurePresent: true });
      }
    } catch (e) {
      
    }
    
  }

  goToStep2 = (step1Details) => {
    this.setState({ isStep2: true, step1Details });
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header 
          centerComponent={{ text: 'Annexure M Details', style: { color: '#fff', fontSize: 16 } }}
          containerStyle={{ paddingTop: 20, height: 80 }}
        />
        {this.state.isAnnexurePresent ? (
          <View style={{ marginTop: 20 }}>
            <Text style={{ alignSelf: 'center', fontSize: 24 }}>You have already filled this form!</Text>
          </View>
        ) : this.state.isStep2 ? 
        (
          <AnnexureForm2 
            step1Details={this.state.step1Details}
          />
        ) : (
          <AnnexureForm1
            goToStep2={this.goToStep2} 
            centerID={this.state.centerID}
            batchID={this.state.batchID}
          />
        )}
      </View>
    )
  }
}

export default AnnexureDetailsScreen;