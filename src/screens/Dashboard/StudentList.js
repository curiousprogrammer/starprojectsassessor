import React, { Component } from 'react';
import { View, Text, FlatList, ScrollView, ActivityIndicator } from 'react-native';
import { CheckBox, Button } from 'react-native-elements';
import { withNavigation } from 'react-navigation';

import globalStyles from '../../common/globalStyles';
import { storeStudents, getStudents } from '../../common/localSave';

class StudentList extends Component {
  constructor(props) {
    super(props);

    const { centerID, assessmentID, assessorID } = this.props.navigation.state.params;

    this.state = {
      loading: true,
      centerID: centerID,
      assessmentID: assessmentID,
      assessorID: assessorID,
      studentList: []
    } 
  }

  async componentDidMount() {
    this.focusListener = this.props.navigation.addListener('willFocus', this.load);
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  load = async () => {
    const { centerID } = this.state;
    const students = await getStudents(centerID);

    this.setState({ studentList: students, loading: false });
  }

  onCheckStudent = (test, index) => {
    const studentListCopy = this.state.studentList;

    studentListCopy[index][test] = !studentListCopy[index][test];

    this.setState({ studentList: studentListCopy });
  }

  onStartTest = (candidate_id) => {
    const details = {
      centerID: this.state.centerID,
      assessmentID: this.state.assessmentID,
      assessorID: this.state.assessorID,
      candidateID: candidate_id
    };
    this.props.navigation.navigate("TestInfo", {  details });
  }

  saveAttendance = () => {
    const { centerID, studentList } = this.state;
    storeStudents(studentList, centerID, false);
  }

  render() {
    return (
      <View style={globalStyles.container}>
        {this.state.loading ? 
          (
            <View>
              <ActivityIndicator size="large" />
            </View>
          ) : (
            <ScrollView style={{ paddingTop: 20 }}>
              <FlatList
                contentContainerStyle={{ paddingBottom: 30 }}
                data={this.state.studentList}
                renderItem={({ item, index }) => 
                (
                  <View
                    style={styles.studentContainer}
                  >
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                      <View style={{ flex: 0.7 }}>
                        <Text style={styles.studentDetails}>{index + 1}. {item.name}</Text>
                        <Text style={styles.studentSubDetails}>{item.email}</Text>
                        <Text style={styles.studentSubDetails}>{item.phone}</Text>
                      </View>
                      <View style={{ flex: 0.3, flexDirection: 'column' }}>
                        <CheckBox
                          containerStyle={{ backgroundColor: "transparent", borderWidth: 0 }}
                          title="Theory"
                          uncheckedIcon='square-o'
                          uncheckedColor="red"
                          checked={item.theory}
                          onPress={() =>this.onCheckStudent("theory", index)}
                        />
                        <CheckBox
                          containerStyle={{ backgroundColor: "transparent", borderWidth: 0 }}
                          title="Practical"
                          uncheckedIcon='square-o'
                          uncheckedColor="red"
                          checked={item.practical}
                          onPress={() => this.onCheckStudent("practical", index)}
                        />
                        <Button
                          title="Start"
                          disabled={item.practical}
                          onPress={() => this.onStartTest(item.candidate_id)}
                        />
                      </View>
                    </View>
                  </View>
                )
              }
              keyExtractor={item => item.student_id}
            />
            <Button 
              title="SAVE"
              containerStyle={{ marginBottom: 50 }}
              onPress={this.saveAttendance}
            />
            </ScrollView>
          )
        }
      </View>
    )
  }
}

export default withNavigation(StudentList);

const styles = {
  studentContainer: {
    borderRadius: 20,    
    borderColor: '#D3D3D3',
    borderWidth : 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    marginVertical: 5
  },
  studentDetails: {
    fontSize: 16,
    marginTop: 15
  },
  studentSubDetails: {
    color: '#919191',
    fontSize: 14,
    paddingLeft: 18
  }
}