import React, { Component } from 'react';
import { View, Image, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';

import globalStyles from '../common/globalStyles';
import Card from '../components/Common/Card';
import CenterVerificationForm from '../components/CenterVerificationForm';
import DownloadAssessmentForm from '../components/DownloadAssessmentForm';

class CenterVerificationScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      verified: false,
      centerDetails: null
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { loading, error, centerDetails } = this.props;
    if (!loading && !error && prevProps.centerDetails !== centerDetails) {
      this.setState({ centerDetails, verified: true });
    }
  }

  setVerified = () => {
    this.setState({ verified: false });
  }

  render() {
    return (
      <View style={globalStyles.container}>
        <Card>
          <View style={styles.logoContainer}>
            <Image
              style={styles.logo}
              source={require('../../assets/img/logo.png')}
            />
          </View>

          <View style={{ alignItems: 'center' }}>
            {!this.state.verified ?
              (
                <CenterVerificationForm />
              ) : (
                <DownloadAssessmentForm 
                  centerDetails={this.state.centerDetails}
                  setVerified={this.setVerified}
                  navigate={this.props.navigation.navigate}
                />
              )
            }
          </View>

          {this.props.loading && 
            (
              <View style={styles.container}>
                <ActivityIndicator size="large" />
              </View>
            )
          }
        </Card>
      </View>

    )
  }
}

const styles = {
  logoContainer: {
    alignItems: 'center'
  },
  container: {
    marginTop: 10,
    marginBottom: 10
  },
  logo: {
    width: 400,
    height: 250,
    resizeMode: 'contain'
  }
}

const mapStateToProps = ({ verification, download }) => {
  return {
    centerDetails: verification.centerDetails,
    loading: verification.loading || download.loading
  }
}

export default connect(mapStateToProps, null)(CenterVerificationScreen);