import {  
  UPLOAD_STUDENT_IMAGE,
  UPLOAD_STUDENT_IMAGE_SUCCESS,
  UPLOAD_STUDENT_IMAGE_FAIL,
  UPLOAD_STUDENT_VIDEO,
  UPLOAD_STUDENT_VIDEO_SUCCESS,
  UPLOAD_STUDENT_VIDEO_FAIL,
  UPLOAD_PRACTICAL_ANSWERS,
  UPLOAD_PRACTICAL_ANSWERS_SUCCESS,
  UPLOAD_PRACTICAL_ANSWERS_FAIL,
  UPLOAD_ANNEXURE,
  UPLOAD_ANNEXURE_SUCCESS,
  UPLOAD_ANNEXURE_FAIL,
  UPLOAD_ANNEXURE_IMAGE,
  UPLOAD_ANNEXURE_IMAGE_SUCCESS,
  UPLOAD_ANNEXURE_IMAGE_FAIL,
} from '../actions/types';

const INITIAL_STATE = {
  loading: false,
  uploadError: false,
  answerUploaded: false,
  imageUploaded: false,
  videoUploaded: false,
  annexureImageUploaded: false
};

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {
    case UPLOAD_PRACTICAL_ANSWERS:
      return { ...state, loading: true, answerUploaded: false, uploadError: false };
    
    case UPLOAD_PRACTICAL_ANSWERS_SUCCESS:
      return { ...state, loading: false, answerUploaded: true, uploadError: false };

    case UPLOAD_PRACTICAL_ANSWERS_FAIL:
      return { ...state, loading: false, answerUploaded: false, uploadError: true };

    case UPLOAD_STUDENT_IMAGE: 
    return { ...state, loading: true, imageUploaded: false, uploadError: false };

    case UPLOAD_STUDENT_IMAGE_SUCCESS:
      return { ...state, loading: false, imageUploaded: true, uploadError: false };

    case UPLOAD_STUDENT_IMAGE_FAIL:
      return { ...state, loading: false, imageUploaded: false, uploadError: true };

    case UPLOAD_STUDENT_VIDEO: 
    return { ...state, loading: true, videoUploaded: false, uploadError: false };

    case UPLOAD_STUDENT_VIDEO_SUCCESS:
      return { ...state, loading: false, videoUploaded: true, uploadError: false };

    case UPLOAD_STUDENT_VIDEO_FAIL:
      return { ...state, loading: false, videoUploaded: false, uploadError: true };

    case UPLOAD_ANNEXURE_IMAGE: 
    return { ...state, loading: true, annexureImageUploaded: false, uploadError: false };

    case UPLOAD_ANNEXURE_IMAGE_SUCCESS:
      return { ...state, loading: false, annexureImageUploaded: true, uploadError: false };

    case UPLOAD_ANNEXURE_IMAGE_FAIL:
      return { ...state, loading: false, annexureImageUploaded: false, uploadError: true };

    case UPLOAD_ANNEXURE: 
    return { ...state, loading: true, uploadError: false };

    case UPLOAD_ANNEXURE_SUCCESS:
      return { ...state, loading: false, uploadError: false };

    case UPLOAD_ANNEXURE_FAIL:
      return { ...state, loading: false, uploadError: true };

    default:
      return state;
  }
}