import {
  SAVE_ANNEXURE_IMAGE,
  SAVE_ANNEXURE_DETAILS,
  SAVE_STUDENT_IMAGE, 
  RESET_SAVED_IMAGE, 
  SAVE_PRACTICAL_ANSWERS,
  RESET_TEST_SAVED
} from '../actions/types';

const INITIAL_STATE = {
  uploadSerial: "",
  annexureSaved: false,
  savedImage: false,
  testSaved: false
};

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {
    case SAVE_ANNEXURE_DETAILS:
      return { ...state, annexureSaved: true }

    case SAVE_ANNEXURE_IMAGE:
      return { ...state, uploadSerial: action.payload };

    case SAVE_STUDENT_IMAGE:
      return { ...state, savedImage: true };

    case RESET_SAVED_IMAGE: {
      return { ...state, savedImage: false };
    }
    
    case SAVE_PRACTICAL_ANSWERS:
      return { ...state, testSaved: true };

    case RESET_TEST_SAVED:
      return { ...state, testSaved: false };

    default:
      return state;
  }
}