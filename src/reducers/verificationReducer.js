import { VERIFY_CENTER, VERIFY_CENTER_SUCCESS, VERIFY_CENTER_FAIL } from '../actions/types';

const INITIAL_STATE = {
  loading: false,
  centerObj: null,
  centerDetails: null,
  error: false,
  errorMsg: null,
  successMsg: null
};

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {
    case VERIFY_CENTER: 
      return { ...state, loading: true, error: false, centerObj: action.payload };

    case VERIFY_CENTER_SUCCESS:
      return { 
        ...state, 
        centerDetails: action.payload.centerDetails, 
        loading: false, 
        error: false, 
        successMsg: action.payload.message
      };

    case VERIFY_CENTER_FAIL:
      return { ...state, loading: false, error: true, errorMsg: action.payload.message };

    default:
      return state;
  }
}