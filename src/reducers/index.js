import { combineReducers } from 'redux';

import verificationReducer from './verificationReducer';
import downloadReducer from './downloadReducer';
import saveReducer from './saveReducer';
import uploadReducer from './uploadReducer';
import testReducer from './testReducer';

export default combineReducers({
  verification: verificationReducer,
  download: downloadReducer,
  save: saveReducer,
  upload: uploadReducer,
  test: testReducer,
});