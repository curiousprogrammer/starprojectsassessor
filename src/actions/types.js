export const VERIFY_CENTER = 'verify_center';
export const VERIFY_CENTER_SUCCESS = 'verify_center_success';
export const VERIFY_CENTER_FAIL = 'verify_center_fail';

export const DOWNLOAD_ASSESSMENT = 'download_assessment';
export const DOWNLOAD_ASSESSMENT_SUCCESS = 'download_assessment_success';
export const DOWNLOAD_ASSESSMENT_FAIL = 'download_assessment_fail';

export const LOGIN_ASSESSOR = 'login_assessor';
export const LOGIN_ASSESSOR_SUCCESS = 'login_assessor_success';
export const LOGIN_ASSESSOR_FAIL = 'login_assessor_fail';



// SAVE
export const SAVE_STUDENT_IMAGE = 'save_student_image';
export const SAVE_STUDENT_VIDEO = 'save_student_video';

export const SAVE_PRACTICAL_ANSWERS = 'save_practical_answers';

export const RESET_SAVED_IMAGE = 'reset_saved_image';

export const SAVE_COMPLETED_STUDENT = 'save_completed_student';

export const SAVE_ANNEXURE_DETAILS = 'save_annexure_details';
export const SAVE_ANNEXURE_IMAGE = 'save_annexure_image';



// UPLOAD
export const UPLOAD_ANNEXURE = 'upload_annexure';
export const UPLOAD_ANNEXURE_SUCCESS = 'upload_annexure_success';
export const UPLOAD_ANNEXURE_FAIL = 'upload_annexure_fail';

export const UPLOAD_ANNEXURE_IMAGE = 'upload_annexure_image';
export const UPLOAD_ANNEXURE_IMAGE_SUCCESS = 'upload_annexure_image_success';
export const UPLOAD_ANNEXURE_IMAGE_FAIL = 'upload_annexure_image_fail';

export const UPLOAD_STUDENT_IMAGE = 'upload_student_image';
export const UPLOAD_STUDENT_IMAGE_SUCCESS = 'upload_student_image_success';
export const UPLOAD_STUDENT_IMAGE_FAIL = 'upload_student_image_fail';

export const UPLOAD_STUDENT_VIDEO = 'upload_student_video';
export const UPLOAD_STUDENT_VIDEO_SUCCESS = 'upload_student_video_success';
export const UPLOAD_STUDENT_VIDEO_FAIL = 'upload_student_video_fail';

export const UPLOAD_PRACTICAL_ANSWERS = 'upload_practical_answers';
export const UPLOAD_PRACTICAL_ANSWERS_SUCCESS = 'upload_practical_answers_success';
export const UPLOAD_PRACTICAL_ANSWERS_FAIL = 'upload_practical_answers_fail';



// TEST
export const GET_TESTS = 'get_tests';
export const SAVE_ASSESSMENT_DETAILS = 'save_assessment_details';
export const RESET_TEST_SAVED = 'reset_test_saved';