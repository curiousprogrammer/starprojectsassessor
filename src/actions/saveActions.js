import {
  SAVE_STUDENT_IMAGE,
  SAVE_STUDENT_VIDEO,
  RESET_SAVED_IMAGE,
  SAVE_PRACTICAL_ANSWERS,
  RESET_TEST_SAVED,
  SAVE_COMPLETED_STUDENT,
  SAVE_ANNEXURE_DETAILS,
  SAVE_ANNEXURE_IMAGE
} from './types';
import { storeStudentImages, storeStudentVideo, storePracticalAnswers, storeCompletedStudent, storeAnnexureDetails, storeAnnexureImages } from '../common/localSave';

export const saveStudentImage = (image, candidate_id, assessment_id) => {
  return async (dispatch) => {
    try {
      dispatch({ type: SAVE_STUDENT_IMAGE });

      const studentImage = {
        image,
        candidate_id,
        assessment_id,
        timeStamp: Date.now()
      }

      storeStudentImages(studentImage);
      
    } catch (e) {
      console.log(e);
    }
  }
}

export const resetSavedImage = () => {
  return {
    type: RESET_SAVED_IMAGE
  }
}

export const saveStudentVideo = (video, candidate_id, assessment_id, assessor_id) => {
  return async (dispatch) => {
    try {
      dispatch({ type: SAVE_STUDENT_VIDEO });

      const studentVideo = {
        video,
        candidate_id,
        assessment_id,
        assessor_id,
        timeStamp: Date.now()
      }

      storeStudentVideo(studentVideo);
      
    } catch (e) {
      console.log(e);
    }
  }
}

export const savePracticalAnswers = (answers, candidate_id, assessment_id) => {
  return async (dispatch) => {
    try {
      dispatch({ type: SAVE_PRACTICAL_ANSWERS });

      const practicalAnswers = {
        answers,
        candidate_id,
        assessment_id
      };

      storePracticalAnswers(practicalAnswers);

      } catch (e) {
      console.log(e);
    }
  }
}

export const resetTestSaved = () => {
  return {
    type: RESET_TEST_SAVED
  }
}

export const saveCompletedStudent = (candidateID, centerID) => {
  return async (dispatch) => {
    try {
      dispatch({ type: SAVE_COMPLETED_STUDENT });

      storeCompletedStudent(candidateID, centerID);

      } catch (e) {
      console.log(e);
    }
  }
}

export const saveAnnexureDetails = (annexureDetails) => {
  return async (dispatch) => {
    try {
      dispatch({ type: SAVE_ANNEXURE_DETAILS });

      storeAnnexureDetails(annexureDetails);

      } catch (e) {
      console.log(e);
    }
  }
}

export const saveAnnexureImage = (image, serial) => {
  return async (dispatch) => {
    try {
      dispatch({ type: SAVE_ANNEXURE_IMAGE, payload: serial });

      const annexureImage = {
        image,
        serial
      };

      storeAnnexureImages(annexureImage);

      } catch (e) {
      console.log(e);
    }
  }
}