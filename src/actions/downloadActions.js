import axios from 'axios';

import { DOWNLOAD_ASSESSMENT, DOWNLOAD_ASSESSMENT_SUCCESS, DOWNLOAD_ASSESSMENT_FAIL } from './types';
import { baseURL } from '../endpoint';
import { setData, setAssessmentData } from '../common/utils';
import { storeStudents } from '../common/localSave';

export const downloadAssessment = (assessmentID, verifyCenterObj) => {
  return async (dispatch) => {
    try {
      dispatch({ type: DOWNLOAD_ASSESSMENT });

      let bodyFormData = new FormData();
      bodyFormData.append('tag', 'download_assessment');
      bodyFormData.append('center_id', verifyCenterObj.centerID);
      bodyFormData.append('assessor_id', verifyCenterObj.assessorID);
      bodyFormData.append('password', verifyCenterObj.assessorPassword);
      bodyFormData.append('assessment_id', assessmentID);
      
      const { data } = await axios({
        method: 'post',
        url: `${baseURL}/service.php`,
        headers: {
          'Content-Type': 'multipart/form-data' 
        },
        data: bodyFormData
      });

      if (data.status == 0) {
        dispatch({ type: DOWNLOAD_ASSESSMENT_FAIL , payload: { message: data.error_msg } });
      } else if (data.status == 1) {
        
        let bodyFormData = new FormData();
        bodyFormData.append('tag', 'download_batch');
        bodyFormData.append('center_id', verifyCenterObj.centerID);
        bodyFormData.append('assessor_id', verifyCenterObj.assessorID);
        bodyFormData.append('password', verifyCenterObj.assessorPassword);
        bodyFormData.append('assessment_id', assessmentID);
        bodyFormData.append('batch_id', data.data.batch.id);
        
        const { data: { batch } } = await axios({
          method: 'post',
          url: `${baseURL}/service.php`,
          headers: {
            'Content-Type': 'multipart/form-data' 
          },
          data: bodyFormData
        });
        
        dispatch({ type: DOWNLOAD_ASSESSMENT_SUCCESS, payload: { assessment: data.data.assessment, batch } });
        setData('assessmentID', data.data.assessment.id);
        setData('center', data.data.center);
        setData('assessorData', data.data.assessor);
        setAssessmentData('assessmentData', data.data);
        setData('batchData', data.data.batch);
        storeStudents(batch.students, data.data.center.id, true);
      }

    } catch (e) {
      console.log(e);
    }
  }
};