import axios from 'axios';
import { 
  UPLOAD_ANNEXURE, 
  UPLOAD_ANNEXURE_SUCCESS, 
  UPLOAD_ANNEXURE_FAIL,
  UPLOAD_ANNEXURE_IMAGE,
  UPLOAD_ANNEXURE_IMAGE_SUCCESS,
  UPLOAD_ANNEXURE_IMAGE_FAIL,
  UPLOAD_STUDENT_IMAGE,
  UPLOAD_STUDENT_IMAGE_SUCCESS,
  UPLOAD_STUDENT_IMAGE_FAIL,
  UPLOAD_STUDENT_VIDEO,
  UPLOAD_STUDENT_VIDEO_SUCCESS,
  UPLOAD_STUDENT_VIDEO_FAIL,
  UPLOAD_PRACTICAL_ANSWERS,
  UPLOAD_PRACTICAL_ANSWERS_SUCCESS,
  UPLOAD_PRACTICAL_ANSWERS_FAIL
} from './types';
import { baseURL } from '../endpoint';

export const uploadAnnexure = (step1Details, step2Details) => {
  return async (dispatch) => {
    try {
      dispatch({ type: UPLOAD_ANNEXURE });

      let bodyFormData = new FormData();
      bodyFormData.append('tag', 'upload_annexure_details');
      bodyFormData.append('agency_name', step1Details.agency_name);
      bodyFormData.append('assessor_name', step1Details.assessor_name);
      bodyFormData.append('assessor_adhar_number', step1Details.assessor_adhar_number);
      bodyFormData.append('no_of_candi_in_batch', step1Details.no_of_candi_in_batch);
      bodyFormData.append('traning_partner_name', step1Details.traning_partner_name);
      bodyFormData.append('traning_center_name', step1Details.traning_center_name);
      bodyFormData.append('spoc_name', step1Details.spoc_name);
      bodyFormData.append('spoc_mobile_no', step1Details.spoc_mobile_no);
      bodyFormData.append('center_id', step1Details.center_id);
      bodyFormData.append('batch_id', step1Details.batch_id);
      bodyFormData.append('job_role', step1Details.job_role);
      bodyFormData.append('sector_name', step1Details.sector_name);
      bodyFormData.append('student_language_for_assessment', step1Details.student_language_for_assessment);
      bodyFormData.append('assement_language', step1Details.assement_language);
      bodyFormData.append('center_address', step1Details.center_address);
      bodyFormData.append('actual_date', step1Details.actual_date);
      bodyFormData.append('actual_center_address', step1Details.actual_center_address);

      for (i=1; i <= 22; i++) {
        const step2Question = `question_${i}`;
        for(j=0; j < 2; j++) {
          bodyFormData.append(`question_${i}[]`, step2Details[step2Question][j]);
        }
      }
      
      const { data } = await axios({
        method: 'post',
        url: `${baseURL}/service.php`,
        headers: {
          'Content-Type': 'multipart/form-data' 
        },
        data: bodyFormData
      });

      if (data.status == 1 ) {
        dispatch({ type: UPLOAD_ANNEXURE_SUCCESS });
      } else if (data.status == 0) {
        dispatch({ type: UPLOAD_ANNEXURE_FAIL });
      }

    } catch (e) {
      console.log(e);
    }
  }
}

export const uploadAnnexureImage = (image, assessmentID) => {
  return async (dispatch) => {
    try {
      dispatch({ type: UPLOAD_ANNEXURE_IMAGE });

      let bodyFormData = new FormData();
      bodyFormData.append('tag', 'upload_annexure_image');
      bodyFormData.append('assessor_assessment_id', assessmentID);
      bodyFormData.append(`q_image_${image.serial}`, image.image);
      
      const { data } = await axios({
        method: 'post',
        url: `${baseURL}/service.php`,
        headers: {
          'Content-Type': 'multipart/form-data' 
        },
        data: bodyFormData
      });

      if (data.status == 1) {
        dispatch({ type: UPLOAD_ANNEXURE_IMAGE_SUCCESS });
      } else if (data.status == 0) {
        dispatch({ type: UPLOAD_ANNEXURE_IMAGE_FAIL});
      }
    } catch (e) {
      console.log(e);
    }
  }
}

export const uploadStudentImage = (image) => {
  return async (dispatch) => {
    try {
      dispatch({ type: UPLOAD_STUDENT_IMAGE });

      let bodyFormData = new FormData();
      bodyFormData.append('tag', 'upload_student_image');
      bodyFormData.append('candidate_id', image.candidate_id);
      bodyFormData.append('assessment_id', image.assessment_id);
      bodyFormData.append('image', image.image);

      const { data } = await axios({
        method: 'post',
        url: `${baseURL}/service.php`,
        headers: {
          'Content-Type': 'multipart/form-data' 
        },
        data: bodyFormData
      });

      if (data.status == 1) {
        dispatch({ type: UPLOAD_STUDENT_IMAGE_SUCCESS });
      } else if (data.status == 0) {
        dispatch({ type: UPLOAD_STUDENT_IMAGE_FAIL });
      }
    } catch (e) {
      console.log(e);
    }
  }
}

export const uploadStudentVideo = (video) => {
  return async (dispatch) => {
    try {
      dispatch({ type: UPLOAD_STUDENT_VIDEO });

      const timestamp = Date.now();

      let bodyFormData = new FormData();
      bodyFormData.append('tag', 'upload_student_video');
      bodyFormData.append('candidate_id', video.candidate_id);
      bodyFormData.append('assessment_id', video.assessment_id);
      bodyFormData.append('assessor_id', video.assessor_id);
      bodyFormData.append('video_data', {
        uri: video.video,
        type: 'video/mp4',
        name: timestamp.toString(),
      });
      
      const { data } = await axios({
        method: 'post',
        url: `${baseURL}/service.php`,
        headers: {
          'Content-Type': 'multipart/form-data' 
        },
        data: bodyFormData
      });

      if (data.status == 1) {
        dispatch({ type: UPLOAD_STUDENT_VIDEO_SUCCESS });
      } else if (data.status == 0) {
        dispatch({ type: UPLOAD_STUDENT_VIDEO_FAIL });
      }
    } catch (e) {
      console.log(e);
    }
  }
}

export const uploadPracticalAnswers = (answers, candidate_id, assessment_id) => {
  return async (dispatch) => {
    try {
      dispatch({ type: UPLOAD_PRACTICAL_ANSWERS });

      let bodyFormData = new FormData();
      bodyFormData.append('tag', 'upload_practical_answer');
      bodyFormData.append('candidate_id', candidate_id);
      bodyFormData.append('assessment_id', assessment_id);
      bodyFormData.append('marks', JSON.stringify(answers));
      
      const { data } = await axios({
        method: 'post',
        url: `${baseURL}/service.php`,
        headers: {
          'Content-Type': 'multipart/form-data' 
        },
        data: bodyFormData
      });

      if (data.status == 1) {
        dispatch({ type: UPLOAD_PRACTICAL_ANSWERS_SUCCESS });
      } else if (data.status == 0) {
        dispatch({ type: UPLOAD_PRACTICAL_ANSWERS_FAIL });
      }
    } catch (e) {
      console.log(e);
    }
  }
}