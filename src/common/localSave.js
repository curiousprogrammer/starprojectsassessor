import AsyncStorage from '@react-native-community/async-storage';

import store from '../store/index';

const storeAnnexureDetails = async (annexureDetails) => {
  try {
    const storageKey = `@annexure_details`;
    storeData(storageKey, annexureDetails);
  } catch (e) {
    console.log(e);
  }
}

const storeAnnexureImages = async (value) => {
  const storageKey = '@annexure_images';
  const existingImages = await getData(storageKey);

  if (existingImages && existingImages.length > 0) {
    existingImages.push(value);
    storeData(storageKey, existingImages);
  } else {
    let annexureImagesArray = [];
    annexureImagesArray.push(value);
    storeData(storageKey, annexureImagesArray);
  }
}

const storeStudentImages = async (value) => {
  const storageKey = '@student_images';
  const existingImages = await getData(storageKey);

  if (existingImages && existingImages.length > 0) {
    existingImages.push(value);
    storeData(storageKey, existingImages);
  } else {
    let studentImagesArray = [];
    studentImagesArray.push(value);
    storeData(storageKey, studentImagesArray);
  }
}

const storeStudentVideo = async (value) => {
  const storageKey = '@student_videos';
  const existingVideos = await getData(storageKey);

  if (existingVideos && existingVideos.length > 0) {
    existingVideos.push(value);
    storeData(storageKey, existingVideos);
  } else {
    let studentVideosArray = [];
    studentVideosArray.push(value);
    storeData(storageKey, studentVideosArray);
  }
}

const storePracticalAnswers = async (value) => {
  const storageKey = '@practical_answers';
  const candidateId = value.candidate_id;
  const existingAnswers = await getData(storageKey);
  let answersObj = {
    [candidateId]: value
  };

  if (existingAnswers) {
    const updatedAnswers = { ...existingAnswers, ...answersObj };
    storeData(storageKey, updatedAnswers);
  } else {
    storeData(storageKey, answersObj);
  }

  storeAllData();
}

const storeAllData = async () => {
  const existingImages = await getData("@student_images");
  const existingVideos = await getData("@student_videos");
  const existingAnswers = await getData("@practical_answers");

  const { centerID, assessmentID, assessmentName, assessmentDuration } = store.getState().test.assessmentDetails;

  const uploadArray = [];
  const uploadData = {
    assessmentDetails: {
      centerID,
      assessmentID,
      assessmentName,
      assessmentDuration,
      uploaded: false
    },
    assessmentData: {
      images: existingImages,
      videos: existingVideos,
      answers: existingAnswers,
    }
  };
  uploadArray.push(uploadData);
  
  const storageKey = '@upload_data';
  const existingUploads = await getData(storageKey);

  let updatedUploads;
  if (existingUploads) {
    existingUploads.forEach(assessment => {
      if (assessment.assessmentDetails.centerID === centerID) {
        assessment.assessmentData = { ...uploadData.assessmentData };
        updatedUploads = existingUploads;
      } else {
        updatedUploads = existingUploads.concat(uploadArray);
      }
    });
    storeData(storageKey, updatedUploads);
  } else {
    storeData(storageKey, uploadArray);
  }
}

const completeAssessment = async (assessmentDetails) => {
  const storageKey = '@upload_data';
  const existingUploads = await getData(storageKey);

  const updatedUploads = existingUploads.map(uploads => {
    if (uploads.assessmentDetails.centerID == assessmentDetails.centerID && uploads.assessmentDetails.assessmentID == assessmentDetails.assessmentID) {
      uploads.assessmentDetails.uploaded = true;
      return {
        assessmentDetails: uploads.assessmentDetails
      }
    } else {
      return {
        ...uploads
      }
    }
  });

  storeData(storageKey, updatedUploads);

  AsyncStorage.multiRemove(["@student_images", "@student_videos", "@practical_answers", "@annexure_images", "@annexure_details"]);

}

const storeStudents = async (students, centerID, initial) => {
  const storageKey = `@student_attendance`;
  const existingAttendance = await getData(storageKey);

  let modifiedStudents;
  if (initial) {
    modifiedStudents = students.map((student) => {
      return {
        ...student,
        theory: false,
        practical: false
      }
    });
  } else {
    modifiedStudents = students;
  }

  let attendanceObj = {
    [centerID]: modifiedStudents
  };

  if (existingAttendance) {
    const updatedAttendance = { ...existingAttendance, ...attendanceObj };
    storeData(storageKey, updatedAttendance);
  } else {
    storeData(storageKey, attendanceObj);
  }
}

const getData = async (storageKey) => {
  try {
    const jsonValue = await AsyncStorage.getItem(storageKey);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch(e) {
    // error reading value
  }
}

const storeData = async (storageKey, value) => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem(storageKey, jsonValue);
  } catch (e) {
    // saving error
  }
}

const getStudents = async (centerID) => {
  try {
    const storageKey = `@student_attendance`;
    const existingAttendance = await getData(storageKey);
    const centerStudents = existingAttendance[centerID];  
    return centerStudents;
  } catch (e) {
    console.log(e);
  }
}

const storeCompletedStudent = async (candidateID, centerID) => {
  try {
    const storageKey = `@student_attendance`;
    const existingAttendance = await getData(storageKey);
    const centerStudents = existingAttendance[centerID];  
    centerStudents.forEach(student => {
      if (student.candidate_id === candidateID) {
        student.practical = true;
      }
    });

    storeStudents(centerStudents, centerID, false);
  } catch (e) {
    console.log(e);
  }
}

export { 
  storeStudentImages,
  storeStudentVideo,
  storePracticalAnswers,
  storeCompletedStudent, 
  storeAnnexureDetails,
  storeAnnexureImages,
  completeAssessment, 
  getData, 
  storeStudents, 
  getStudents 
};