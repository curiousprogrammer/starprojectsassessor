export default {
  container: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 20
  },
  spaceBetween: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
}